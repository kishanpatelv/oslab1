package in.ac.vit.oslab.producer;

public class Producer extends Thread {

	private String nextItem;
	private String[] buffer;
	private int pointer;
	private int itemsToProduce;
	private int speed;

	public Producer(String[] buffer, int itemsToProduce, int speed) {
		super();
		this.buffer = buffer;
		this.itemsToProduce = itemsToProduce;
		this.pointer = 0;
		this.speed = speed;
	}

	public void incrementPointer() {
		this.pointer = (this.pointer++) % this.buffer.length;
	}

	public boolean putItemIntoBuffer() {
		if (this.buffer[this.pointer].equals("")) {
			this.buffer[this.pointer] = this.nextItem;
			this.incrementPointer();
			this.nextItem = "";
			return true;
		}
		return false;
	}

	@Override
	public void run() {
		System.out.println("Producer Thread Started...");
		for (int i = 1; i <= this.itemsToProduce; i++) {
			this.nextItem = "Item " + i;
			System.out.println("Produced -->" + this.nextItem);
			try {
				while (!this.putItemIntoBuffer()) {
					System.out
							.println("Producer : Buffer is Full. Will try again after 1 sec..");
					Thread.sleep(1000);
				}
				Thread.sleep(this.speed * 1000);
			} catch (InterruptedException e) {
				System.err.println("Producer Method interrupted");
			}
		}
	}
}
