package in.ac.vit.oslab.consumer;

public class Consumer extends Thread {

	private String nextItem;
	private String[] buffer;
	private int pointer;
	private int itemsToConsume;
	private int speed;

	public Consumer(String[] buffer, int itemsToConsume, int speed) {
		super();
		this.buffer = buffer;
		this.itemsToConsume = itemsToConsume;
		this.speed = speed;
		this.pointer = 0;
	}

	public boolean consumeItemFromBuffer() {
		if (!this.buffer[this.pointer].equals("")) {
			this.nextItem = this.buffer[this.pointer];
			this.buffer[this.pointer] = "";
			this.incrementPointer();
			System.out.println("Consumed --> " + this.nextItem);
			this.nextItem = "";
			return true;
		}
		return false;
	}

	public void incrementPointer() {
		this.pointer = (this.pointer++) % this.buffer.length;
	}

	@Override
	public void run() {
		System.out.println("Consumer Thread Started...");
		for (int i = 1; i < this.itemsToConsume; i++) {
			try {
				while (!this.consumeItemFromBuffer()) {
					System.out
							.println("Consumer : Buffer is Empty. Will try again after 1 sec..");
					Thread.sleep(1000);
				}
				Thread.sleep(this.speed * 1000);
			} catch (InterruptedException e) {
				System.err
						.println("Consumer : My Thread has been interrupted..");
			}
		}
	}

}
