package in.ac.vit.oslab;

import in.ac.vit.oslab.consumer.Consumer;
import in.ac.vit.oslab.producer.Producer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class RunConsumerProducer {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(
					System.in));
			System.out.print("Enter No. of Items to produce : ");

			int itemsToProduce = Integer.parseInt(br.readLine());
			System.out.print("Enter Producer Speed sec/item : ");
			int producerSpeed = Integer.parseInt(br.readLine());
			System.out.print("Enter Consumer Speed sec/item : ");
			int consumerSpeed = Integer.parseInt(br.readLine());
			System.out.print("Enter Buffer Size : ");
			int bufferSize = Integer.parseInt(br.readLine());
			String[] buffer = new String[bufferSize];
			for (int i = 0; i < bufferSize; i++) {
				buffer[i] = "";
			}
			Producer producer = new Producer(buffer, itemsToProduce,
					producerSpeed);
			producer.start();
			Consumer consumer = new Consumer(buffer, itemsToProduce,
					consumerSpeed);
			consumer.start();

		} catch (NumberFormatException | IOException e) {
			e.printStackTrace();
		}
	}

}
