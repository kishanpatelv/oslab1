/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package in.ac.vit.oslab.cpuscheduling;

import java.util.ArrayList;

/**
 * 
 * @author sahir
 */
public class MyProcess implements Comparable<MyProcess> {

	private String name;
	private int arrivalTime;
	private int timeCompleted = 0;
	private int burstTime;
	private int priority = 0;
	private ArrayList<Integer> gantt;

	public MyProcess(String name, int arrivalTime, int burstTime) {
		this.name = name;
		this.arrivalTime = arrivalTime;
		this.burstTime = burstTime;
		this.gantt = new ArrayList<Integer>(this.burstTime + this.arrivalTime);
	}

	public MyProcess(String name, int arrivalTime, int burstTime, int priority) {
		this.name = name;
		this.arrivalTime = arrivalTime;
		this.burstTime = burstTime;
		this.priority = priority;
		this.gantt = new ArrayList<Integer>(this.burstTime + this.arrivalTime);
	}

	@Override
	public int compareTo(MyProcess o) {
		return ((Integer) this.getArrivalTime()).compareTo(o.getArrivalTime());
	}

	public int getArrivalTime() {
		return this.arrivalTime;
	}

	public int getBurstTime() {
		return this.burstTime;
	}

	public ArrayList<Integer> getGantt() {
		return this.gantt;
	}

	public String getName() {
		return this.name;
	}

	public int getPriority() {
		return this.priority;
	}

	public int getRemainingTime() {
		return this.getBurstTime() - this.getTimeCompleted();
	}

	public int getTimeCompleted() {
		return this.timeCompleted;
	}

	public int getTurnAroundTime() {
		return this.gantt.get(this.gantt.size() - 1);
	}

	public int getWaitingTime() {

		int lastQuantaSize = 0;
		int lastIndex = this.gantt.size() - 1;
		while ((lastIndex != 0)
				&& ((this.gantt.get(lastIndex) - this.gantt.get(lastIndex - 1)) == 1)) {
			lastIndex--;
			lastQuantaSize++;
		}
		int completionTime = this.gantt.get(lastIndex);
		return (completionTime - (this.burstTime - lastQuantaSize));

	}

	public void printGantt(int spaces) {
		String value = "=";
		for (int i = 1; i < spaces; i++) {
			value += "=";
		}
		for (int i = 0; i < this.gantt.size(); i++) {
			if (this.gantt.get(i) == 1) {
				System.out.print(value);
			}
			System.out.println("");
		}
	}

	/**
	 * Function to execute process
	 * 
	 * @param timeNow
	 *            - Current CPU time
	 * @param time
	 *            - no of CPU time slots to execute. -1 means execute entire
	 *            process
	 * @return - time after process has been executed
	 */
	public int run(int timeNow, int time) {

		int timeToRun = this.burstTime;
		if (time != -1) {
			// preemptive mode. run for time quanta or less if remaining time is
			// less than quanta size
			timeToRun = time <= (this.burstTime - this.timeCompleted) ? time
					: (this.burstTime - this.timeCompleted);
		}

		for (int i = 0; i < timeToRun; i++) {
			this.gantt.add(timeNow++);
			this.timeCompleted++;
		}
		return timeNow;
	}

	public void setArrivalTime(int arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public void setBurstTime(int burstTime) {
		this.burstTime = burstTime;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}
}
