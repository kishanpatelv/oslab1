/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package in.ac.vit.oslab.cpuscheduling.fcfs;

import in.ac.vit.oslab.cpuscheduling.MyProcess;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/**
 * 
 * @author sahir
 */
public class FCFS {

	private ArrayList<MyProcess> processes;
	private int totalTime;
	private int timeNow = 1;

	public FCFS(ArrayList<MyProcess> process) {
		this.processes = process;
	}

	public ArrayList<MyProcess> getProcesses() {
		return this.processes;
	}

	private int getStartTime() {
		Collections.sort(this.processes);
		return this.processes.get(0).getArrivalTime();
	}

	public int getTotalTime() {
		return this.totalTime;
	}

	public ArrayList<MyProcess> run() {
		this.timeNow = this.getStartTime();
		if (this.timeNow == 0) {
			this.timeNow = 1;
		}
		Iterator<MyProcess> it = this.processes.iterator();
		while (it.hasNext()) {
			MyProcess p = it.next();
			if (p.getArrivalTime() > this.timeNow) {
				// fast forwarding current time
				this.timeNow = p.getArrivalTime();
			}
			this.timeNow = p.run(this.timeNow, -1);
		}
		this.totalTime = this.timeNow - 1;
		return this.processes;
	}
}
