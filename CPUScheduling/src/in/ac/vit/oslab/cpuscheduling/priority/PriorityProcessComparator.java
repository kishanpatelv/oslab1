package in.ac.vit.oslab.cpuscheduling.priority;

import in.ac.vit.oslab.cpuscheduling.MyProcess;

import java.util.Comparator;

public class PriorityProcessComparator implements Comparator<MyProcess> {

	@Override
	public int compare(MyProcess o1, MyProcess o2) {
		int result = 0;
		if (o1.getPriority() != o2.getPriority()) {
			if (o1.getPriority() < o2.getPriority()) {
				result = -1;
			} else {
				result = 1;
			}
		} else {
			if (o1.getArrivalTime() < o2.getArrivalTime()) {
				result = -1;
			} else if (o1.getArrivalTime() > o2.getArrivalTime()) {
				result = 1;
			}
		}
		return result;
	}
}
