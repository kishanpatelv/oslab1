package in.ac.vit.oslab.cpuscheduling.servlets;

import in.ac.vit.oslab.cpuscheduling.MyProcess;
import in.ac.vit.oslab.cpuscheduling.priority.Priority;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

/**
 * Servlet implementation class SJF
 */
@WebServlet("/priority.do")
public class PriorityServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String[] names = request.getParameterValues("name");
		String[] arrivalTimes = request.getParameterValues("arrivalTime");
		String[] burstTimes = request.getParameterValues("burstTime");
		String[] priorities = request.getParameterValues("priority");
		ArrayList<MyProcess> processes = new ArrayList<MyProcess>();
		int i = 0;
		for (String name : names) {
			if (!StringUtils.isBlank(name)) {
				MyProcess p = new MyProcess(name,
						Integer.parseInt(arrivalTimes[i]),
						Integer.parseInt(burstTimes[i]),
						Integer.parseInt(priorities[i]));
				processes.add(p);
			}
			i++;
		}
		Priority priority = new Priority(processes);
		processes = priority.run();
		request.setAttribute("processes", processes);
		request.setAttribute("totalTime", priority.getTotalTime());
		request.setAttribute("name", "Priority");
		request.getRequestDispatcher("results.jsp").forward(request, response);
	}
}
