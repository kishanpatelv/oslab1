package in.ac.vit.oslab.cpuscheduling.servlets;

import in.ac.vit.oslab.cpuscheduling.MyProcess;
import in.ac.vit.oslab.cpuscheduling.rr.RoundRobin;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

/**
 * Servlet implementation class SJF
 */
@WebServlet("/roundrobin.do")
public class RoundRobinServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String[] names = request.getParameterValues("name");
		String[] arrivalTimes = request.getParameterValues("arrivalTime");
		String[] burstTimes = request.getParameterValues("burstTime");
		String quantum = request.getParameter("quantum");

		ArrayList<MyProcess> processes = new ArrayList<MyProcess>();
		int i = 0;
		for (String name : names) {
			if (!StringUtils.isBlank(name)) {
				MyProcess p = new MyProcess(name,
						Integer.parseInt(arrivalTimes[i]),
						Integer.parseInt(burstTimes[i]));
				processes.add(p);
			}
			i++;
		}
		RoundRobin rr = new RoundRobin(processes, Integer.parseInt(quantum));
		processes = rr.run();
		request.setAttribute("processes", processes);
		request.setAttribute("totalTime", rr.getTotalTime());
		request.setAttribute("name", "Round Robin with quantum size = "
				+ quantum);
		request.getRequestDispatcher("results.jsp").forward(request, response);
	}
}
