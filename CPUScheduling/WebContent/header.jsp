<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="baseUrl" value="${pageContext.request.contextPath}"
	scope="request" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>CPU Scheduling Algorithms : Operating Systems</title>
<script type="text/javascript" src="${baseUrl}/resources/jquery.min.js"></script>
<link class="include" rel="stylesheet" type="text/css" href="${baseUrl}/resources/css/style.css" />
<link class="include" rel="stylesheet" type="text/css" href="${baseUrl}/resources/jqplot/jquery.jqplot.min.css" />
<script type="text/javascript">
	var BASE_URL = "${baseUrl}";
	$(function() {
		$("tr:even").addClass("even");
		$("tr:odd").addClass("odd");
	});
</script>
</head>
<body>
	<h1>CPU Scheduling Algorithms</h1>
	<a href="${baseUrl}/index.jsp">Home</a>
	<br />