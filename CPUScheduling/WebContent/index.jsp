<jsp:include page="./header.jsp" />
<ul>
	<li>Preemptive
		<ol>
			<li><a href="${baseUrl}/rr.jsp">Round Robin</a></li>
			<li><a href="${baseUrl}/ppriority.jsp">Priority</a></li>
		</ol>
	</li>
	<li>Non Preemptive
		<ol>
			<li><a href="${baseUrl}/fcfs.jsp">FCFS</a></li>
			<li><a href="${baseUrl}/priority.jsp">Priority</a></li>
			<li><a href="${baseUrl}/sjf.jsp">SJF</a></li>
		</ol>
	</li>
</ul>
</body>
</html>