<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="./header.jsp"/>

<h1>${name} Results : </h1>
<h3>Total Time : <b>${totalTime }</b></h3>
<div class="ganttContainer" style="width: ${totalTime * 32 + 75 + 340 }px">
<div class="processName">Process</div>
<c:forEach begin="1" end="${totalTime}" var="val">
	<div class="gantt"><c:out value="${val}"/></div>
</c:forEach>
<div class="processDetails">Waiting Time</div>
<div class="processDetails">Turn Around Time</div>
<div class="clearfix"></div>
<c:forEach items="${processes}" var="process">
	<div class="processName">${process.name}</div>
	<c:forEach begin="1" end="${totalTime}" var="val">
		<c:choose>
			<c:when test="${process.gantt.contains(val) }">
				<div class="activeGantt"></div>
			</c:when>
			<c:otherwise>
				<div class="gantt"></div>
			</c:otherwise>
		</c:choose>
	</c:forEach>
	<div class="processDetails">${process.getWaitingTime()}</div>
	<div class="processDetails">${process.getTurnAroundTime()}</div> 
	<div class="clearfix"></div>
</c:forEach>
</div>
</body>
</html>